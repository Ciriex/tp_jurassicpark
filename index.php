<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('ma_valeur', "Hello There!");
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    $data = [
        'dinosaures' => getDinosaures(),
        
    ];
    Flight::render('dinos.twig', $data);
});

Flight::route('/detail/@name', function($name){
    $data = [
        'dinosaure' => getOne($name),
    ];
    Flight::render('dino.twig', $data);
});

Flight::start();