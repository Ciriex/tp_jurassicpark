<?php 
require "vendor/autoload.php";
use Michelf\Markdown;
function getDinosaures()
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getOne($name)
{
    $response = Requests::get("https://medusa.delahayeyourself.info/api/dinosaurs/$name");
    $dino = json_decode($response->body);

    //var_dump ($dino) ;
    return $dino ;   
}



function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}